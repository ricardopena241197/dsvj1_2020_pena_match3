#ifndef SOUNDS_H
#define SOUNDS_H
#include "raylib.h"

namespace sounds
{
	extern Music mainMenu;	
	extern Music gamePlay;
	extern bool musicActive;
	extern bool soundsActive;

	extern Sound movingMenu;
	extern bool playMovingMenu;

	extern Sound acceptMenu;
	extern bool playAccepMenu;

	extern float musicVolume;
	extern float soundsVolume;

	const float defaulVolume = 0.0f;

	void initSounds();	
	void deinitSounds();
	void playSounds();	
	void muteUnmuteMusic();
	void muteUnmuteEfects();
	void levelUpMusicVolume();
	void levelDownMusicVolume();
	void levelUpSoundVolume();
	void levelDownSoundVolume();
}

#endif // !SOUNDS_H

