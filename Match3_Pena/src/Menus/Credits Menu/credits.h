#ifndef CREDITS_H
#define CREDITS_H
#include "raylib.h"

namespace credits
{
	enum class menuOptions { Exit };

	const int optionsAmount = 1;
	const int maximumAmountOfCharacters = 100;

	const Color defaultTitleColor = BLACK;
	const Color defaultOptionsColor = BLACK;
	const Color alternativeOptionsColor = RED;

	void draw();
	void deinit();
	void menu();
}

#endif // !CREDITS_H

