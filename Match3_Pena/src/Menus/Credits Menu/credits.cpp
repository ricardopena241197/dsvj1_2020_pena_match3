#include "credits.h"
#include "Menus/Main Menu/main_menu.h"
#include "Controls/controls.h"
#include "Text/text.h"
#include "Game/game.h"
#include "Textures/textures.h"
#include "Global Variables/global_variables.h"
#include "Sounds/sounds.h"

#include <iostream>
#include <string.h>


namespace credits
{
	int spaceBetweenOptions;

	bool initializedMenu = false;

	string credits;
	Vector2 posCredits;
	int creditsSize;

	MenuOptions title;
	MenuOptions options[optionsAmount];
	Color colorOption = BLACK;

	Texture2D menuBackground;
	Vector2 menuBackgroundPos;
	Color menuBackgroundDye;

	int iAuxCursor = 0;

	namespace initialize
	{
		void initializeBackground()
		{
			using namespace textures;

			menuBackground = LoadTexture(menuBackgroundTexturePath);
			menuBackground.height = window::screen.height;
			menuBackground.width = window::screen.width;
			menuBackgroundPos.x = 0;
			menuBackgroundPos.y = 0;
			menuBackgroundDye = WHITE;

		}

		void initializeTitle()
		{
			title.pos.y = window::screen.height / 100;
			title.textSize = window::screen.width / 15;
			title.color = defaultTitleColor;
			title.texture = LoadTexture(textures::optionsTexturePath);
			title.dye = WHITE;
			title.text = "Credits";
			title.size = text::measureStringText(title.text, title.textSize);
			title.pos.x = window::screen.width / 2 - title.size / 2;
			textures::initTitleTexture(title.texture, title);
		}

		void initializeOptions()
		{
			spaceBetweenOptions = window::screen.height / (optionsAmount + 1);

			options[0].pos.y = window::screen.height-(window::screen.height / 4);
			options[0].textSize = window::screen.width / 20;
			options[0].color = defaultOptionsColor;
			options[0].texture = LoadTexture(textures::optionsTexturePath);
			options[0].dye = WHITE;
			options[0].text = "Back";
			options[0].size = text::measureStringText(options[0].text, options[0].textSize);
			options[0].pos.x = window::screen.width / 2 - options[0].size / 2;
			textures::initOptionsTexture(options[0].texture, options[0]);
			
		}

		void inicializeCredits()
		{
			posCredits.y = title.pos.y + title.textSize * 2;
			posCredits.x = window::screen.width/100;
			creditsSize = window::screen.width/30;
			credits = "Development: Ricardo Pena.\n Art: Ricardo Pena, Tim Kaminski y Raphael Lacoste.\n Music: Nazar Rybak.";
		}

	}

	namespace drawing
	{
		void drawBackground()
		{
			DrawTexture(menuBackground, menuBackgroundPos.x, menuBackgroundPos.y, menuBackgroundDye);
		}

		void drawTitle()
		{
			char auxTexto[maximumAmountOfCharacters];
			strcpy_s(auxTexto, title.text.c_str());
			DrawTexture(title.texture, title.pos.x - title.size / 10, title.pos.y - title.textSize / 10, title.dye);
			DrawText(auxTexto, title.pos.x, title.pos.y, title.textSize, title.color);
		}

		void drawOptions()
		{
			using namespace textures;
			char auxTexto[maximumAmountOfCharacters];

			for (short i = 0; i < optionsAmount; i++)
			{
				if (iAuxCursor == i)
				{
					options[i].color = alternativeOptionsColor;
				}
				else
				{
					options[i].color = defaultOptionsColor;
				}

				strcpy_s(auxTexto, options[i].text.c_str());

				DrawTexture(options[i].texture, options[i].pos.x - buttonBorderHeight / 2, options[i].pos.y - buttonBorderHeight / 4, options[i].dye);
				DrawText(auxTexto, options[i].pos.x, options[i].pos.y, options[i].textSize, options[i].color);
			}
		}

		void drawCredits()
		{			
			char auxText[maximumAmountOfCharacters];
			strcpy_s(auxText, credits.c_str());
			DrawText(auxText, posCredits.x, posCredits.y, creditsSize , RAYWHITE);
		}
	}

	static void init()
	{
		using namespace initialize;

		initializeBackground();
		initializeTitle();
		initializeOptions();
		inicializeCredits();

		iAuxCursor = 0;
	}

	static void update()
	{
		using namespace controls;


		if (IsKeyPressed(keyUp)) { iAuxCursor = (iAuxCursor == 0) ? optionsAmount - 1 : iAuxCursor - 1; sounds::playMovingMenu = true;}
		if (IsKeyPressed(keyDown)) { iAuxCursor = (iAuxCursor == optionsAmount - 1) ? 0 : iAuxCursor + 1; sounds::playMovingMenu = true;}
		if (IsKeyPressed(keyEnter))
		{
			sounds::playAccepMenu=true;

			deinit();
			switch ((menuOptions)iAuxCursor)
			{			
			case menuOptions::Exit:
				game::actualScene = Scenes::MainMenu;
				break;
			default:
				break;
			}

		}
	}

	void draw()
	{
		using namespace drawing;

		drawBackground();

		drawTitle();

		drawOptions();

		drawCredits();
	}

	void deinit()
	{
		initializedMenu = false;
		UnloadTexture(title.texture);
		UnloadTexture(options[0].texture);
	}

	void menu()
	{
		if (!initializedMenu)
		{
			init();
			initializedMenu = true;
		}
		update();
	}
}