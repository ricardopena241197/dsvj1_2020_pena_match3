#ifndef PAUSE_MENU
#define PAUSE_MENU
#include "raylib.h"
#include "Window/window.h"

#include <iostream>
#include <string.h>

using namespace std;


namespace pause_menu
{
	enum class menuOptions { Return, Restart, GoMenu, MuteUnmuteMusic, Exit };
	
	const int optionsAmount = 5;
	const int maximumAmountOfCharacters = 20;

	const Color defaultTitleColor = BLACK;
	const Color defaultOptionsColor = BLACK;
	const Color alternativeOptionsColor = RED;
			
	void draw();
	void deinit();
	void menu();
}

#endif // !MAIN_MENU

