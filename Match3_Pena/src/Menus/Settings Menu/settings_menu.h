#ifndef SETTINGS_MENU
#define SETTINGS_MENU
#include "raylib.h"
#include "Window/window.h"

#include <iostream>
#include <string.h>

using namespace std;

namespace settings_menu
{
	enum class menuOptions { Music, Sounds, Graphics, Back};
	
	const int optionsAmount = 4;
	const int maximumAmountOfCharacters = 50;

	const Color defaultTitleColor = BLACK;
	const Color defaultOptionsColor = BLACK;
	const Color alternativeOptionsColor = RED;
			
	void draw();
	void deinit();
	void menu();
}

#endif // !MAIN_MENU

