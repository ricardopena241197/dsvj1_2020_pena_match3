#ifndef MUSIC_MENU
#define MUSIC_MENU
#include "raylib.h"
#include "Window/window.h"

#include <iostream>
#include <string.h>

using namespace std;

namespace music_menu
{
	enum class menuOptions { LevelUp, LevelDown, MuteUnmute, Back};
	
	const int optionsAmount = 4;
	const int maximumAmountOfCharacters = 50;

	const Color defaultTitleColor = BLACK;
	const Color defaultOptionsColor = BLACK;
	const Color alternativeOptionsColor = RED;
			
	void draw();
	void deinit();
	void menu();
}

#endif // !MAIN_MENU

