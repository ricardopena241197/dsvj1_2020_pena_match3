#ifndef GRAPHICS_MENU
#define GRAPHICS_MENU
#include "raylib.h"
#include "Window/window.h"

#include <iostream>
#include <string.h>

using namespace std;

namespace graphics_menu
{
	enum class menuOptions { SD, qHD, HD, FHD, Back};
	
	const int optionsAmount = 5;
	const int maximumAmountOfCharacters = 50;

	const Color defaultTitleColor = BLACK;
	const Color defaultOptionsColor = BLACK;
	const Color alternativeOptionsColor = RED;
			
	void draw();
	void deinit();
	void menu();
}

#endif // !MAIN_MENU

