#ifndef TEXT_H
#define TEXT_H

#include "Menus/Main Menu/main_menu.h"

#include <iostream>
#include <string.h>

using namespace std;

namespace text
{
	int measureStringText(string text, int textSize);
}

#endif // !TEXT_H

