#ifndef TEXTURES_H
#define TEXTURES_H

#include "Menus/Main Menu/main_menu.h"
#include "raylib.h"

namespace textures
{
	//File path for texture assets

	//Gameplay
		//Stage	
	const char backgroundTexturePath[] = "res/assets/textures/Gameplay/background.png";	
	const char pauseButtonTexturePath[] = "res/assets/textures/Gameplay/pause_button.png";

		//Jewels
	const char blueJTexturePath[] = "res/assets/textures/Gameplay/blue.png";
	const char greenJTexturePath[] = "res/assets/textures/Gameplay/green.png";
	const char redJTexturePath[] = "res/assets/textures/Gameplay/red.png";
	const char greyJTexturePath[] = "res/assets/textures/Gameplay/grey.png";

	//Menus
	const char optionsTexturePath[] = "res/assets/textures/menus/options_texture.png";
	const char menuBackgroundTexturePath[] = "res/assets/textures/menus/background.png";

	//UI
		//Mouse
	const char mouseTexturePath[] = "res/assets/textures/UI/mouse.png";

	//Others

	extern int	buttonBorderHeight;
	void initTitleTexture(Texture2D& texture, MenuOptions option);
	void initOptionsTexture(Texture2D& texture, MenuOptions option);
}

#endif // !TEXTURES_H

