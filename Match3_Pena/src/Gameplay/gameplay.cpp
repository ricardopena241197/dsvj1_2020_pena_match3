#include "gameplay.h"
#include "Game/game.h"
#include "Window/window.h"
#include "Textures/textures.h"
#include "Mouse/mouse.h"
#include "Gameplay/Entities/jewels.h"
#include "Text/text.h"
#include "Sounds/sounds.h"

#include <iostream>
#include <stdlib.h>

using namespace std;

namespace gameplay
{
	bool gameInitialized = false;

	Buttons pauseButon;

	Texture2D background;
	Vector2 backgroundPos;
	Color backgoundDye;

	Texture2D blueJewel;
	Texture2D redJewel;
	Texture2D greenJewel;
	Texture2D greyJewel;

	Backboard gameBackboard;

	Vector2 jewelsSelectedPos[amountOfJewels * amountOfJewels];
	int auxIndex;
	bool jewelSelected;

	float turnTimer;
	float regenerateTimer;	

	bool gameOver;

	namespace initialize
	{
		static void initBackground()
		{
			using namespace textures;

			background = LoadTexture(backgroundTexturePath);
			background.height = window::screen.height;
			background.width = window::screen.width;

			backgroundPos.x = 0;
			backgroundPos.y = 0;

			backgoundDye = WHITE;
		}

		static void initJewels()
		{			
			using namespace textures;
			blueJewel = LoadTexture(blueJTexturePath);
			redJewel = LoadTexture(redJTexturePath);
			greenJewel = LoadTexture(greenJTexturePath);
			greyJewel = LoadTexture(greyJTexturePath);

			Vector2 auxPos ;
			auxPos.x = window::screen.width / 90;
			auxPos.y = window::screen.width / 50;

			for (short i = 0; i < amountOfJewels; i++)
			{
				for (short j = 0; j < amountOfJewels; j++)
				{
					jewels::generateJewelType(gameBackboard.jewels[i][j]);

					gameBackboard.jewels[i][j].collider.width = (window::screen.width / 2) / amountOfJewels;
					gameBackboard.jewels[i][j].collider.height = gameBackboard.jewels[i][j].collider.width;

					gameBackboard.jewels[i][j].collider.x = auxPos.x;
					gameBackboard.jewels[i][j].collider.y = auxPos.y;

					gameBackboard.jewels[i][j].dye = WHITE;


					auxPos.x += gameBackboard.jewels[i][j].collider.width;
				}
				auxPos.x = window::screen.width / 90;
				auxPos.y += gameBackboard.jewels[i][0].collider.height;
			}

			blueJewel.height = gameBackboard.jewels[0][0].collider.height;
			blueJewel.width = gameBackboard.jewels[0][0].collider.width;

			redJewel.height = gameBackboard.jewels[0][0].collider.height;
			redJewel.width = gameBackboard.jewels[0][0].collider.width;

			greenJewel.height = gameBackboard.jewels[0][0].collider.height;
			greenJewel.width = gameBackboard.jewels[0][0].collider.width;

			greyJewel.height = gameBackboard.jewels[0][0].collider.height;
			greyJewel.width = gameBackboard.jewels[0][0].collider.width;

		}

		static void initObjetives()
		{
			//Blue Jewels
			gameBackboard.blueObj.amountToReach = 20;

			gameBackboard.blueObj.collider.x = (window::screen.width / 3) * 2;
			gameBackboard.blueObj.collider.y = (window::screen.height / 16) * 2;
			gameBackboard.blueObj.collider.height = window::screen.height / 10;
			gameBackboard.blueObj.collider.width = gameBackboard.blueObj.collider.height;

			gameBackboard.blueObj.texture = LoadTexture(textures::blueJTexturePath);;
			gameBackboard.blueObj.texture.height = gameBackboard.blueObj.collider.height;
			gameBackboard.blueObj.texture.width = gameBackboard.blueObj.texture.height;


			//Red Jewels
			gameBackboard.redObj.amountToReach = 20;

			gameBackboard.redObj.collider.x = (window::screen.width / 3) * 2;
			gameBackboard.redObj.collider.y = (window::screen.height / 16) * 4;
			gameBackboard.redObj.collider.height = gameBackboard.blueObj.collider.height;
			gameBackboard.redObj.collider.width = gameBackboard.blueObj.collider.height;

			gameBackboard.redObj.texture = LoadTexture(textures::redJTexturePath);;
			gameBackboard.redObj.texture.height = gameBackboard.blueObj.collider.height;
			gameBackboard.redObj.texture.width = gameBackboard.blueObj.texture.height;


			//Green Jewels
			gameBackboard.greenObj.amountToReach = 20;

			gameBackboard.greenObj.collider.x = (window::screen.width / 3) * 2;
			gameBackboard.greenObj.collider.y = (window::screen.height / 16) * 6;
			gameBackboard.greenObj.collider.height = gameBackboard.blueObj.collider.height;
			gameBackboard.greenObj.collider.width = gameBackboard.blueObj.collider.height;

			gameBackboard.greenObj.texture = LoadTexture(textures::greenJTexturePath);
			gameBackboard.greenObj.texture.height = gameBackboard.blueObj.collider.height;
			gameBackboard.greenObj.texture.width = gameBackboard.blueObj.texture.height;


			//Grey Jewels
			gameBackboard.greyObj.amountToReach = 20;

			gameBackboard.greyObj.collider.x = (window::screen.width / 3) * 2;
			gameBackboard.greyObj.collider.y = (window::screen.height / 16) * 8;
			gameBackboard.greyObj.collider.height = gameBackboard.blueObj.collider.height;
			gameBackboard.greyObj.collider.width = gameBackboard.blueObj.collider.height;

			gameBackboard.greyObj.texture = LoadTexture(textures::greyJTexturePath);
			gameBackboard.greyObj.texture.height = gameBackboard.blueObj.collider.height;
			gameBackboard.greyObj.texture.width = gameBackboard.blueObj.texture.height;

			gameBackboard.texSize = gameBackboard.blueObj.collider.height;
		}

		static void initBackboard()
		{
			gameBackboard.moves = 20;

			gameBackboard.movesPos.x = (window::screen.width / 3) * 2;
			gameBackboard.movesPos.y = (window::screen.height / 16) * 10;			

			gameBackboard.score = 0;

			gameBackboard.scorePos.x = (window::screen.width / 3) * 2;
			gameBackboard.scorePos.y = (window::screen.height / 16) * 12;

			gameBackboard.level = 1;

			gameBackboard.levelPos.x = (window::screen.width / 3) * 2;
			gameBackboard.levelPos.y = (window::screen.height / 16) * 14;
			
			initObjetives();

			initJewels();
		}

		static void initSelectedPos()
		{
			for (short i = 0; i < amountOfJewels * amountOfJewels; i++)
			{
				jewelsSelectedPos[i].x = 0;
				jewelsSelectedPos[i].y = 0;
			}
			auxIndex = 0;
		}

		static void initTimer(float &actualTimer)
		{
			actualTimer = 0;
		}

		static void initPauseButon()
		{
			pauseButon.collider.height = window::screen.height / 15;
			pauseButon.collider.width = pauseButon.collider.height;
			pauseButon.collider.x = window::screen.width - pauseButon.collider.width;
			pauseButon.collider.y = 0;

			pauseButon.texture = LoadTexture(textures::pauseButtonTexturePath);
			pauseButon.texture.height = pauseButon.collider.height;
			pauseButon.texture.width = pauseButon.collider.width;
		}
	}

	namespace updating
	{
		static bool checkJewelMouseCollision(Vector2 mousePosition,Jewel jewel)
		{
			if (mousePosition.x >= jewel.collider.x && mousePosition.x <= jewel.collider.x+jewel.collider.width)
			{
				if (mousePosition.y >= jewel.collider.y && mousePosition.y <= jewel.collider.y + jewel.collider.height)
				{
					return true;
				}
			}

			return false;
		}

		static bool checkJewelsSelected(int i,int j)
		{
			int auxX = jewelsSelectedPos[auxIndex].x;
			int auxY = jewelsSelectedPos[auxIndex].y;


			if (gameBackboard.jewels[auxY][auxX].type == gameBackboard.jewels[i][j].type)
			{
				if (abs(i - auxY) <= 1 && abs(j - auxX) <= 1)
				{
					return true;
				}				
			}
			return false;			
		}

		static void jewelSelection()
		{
			for (short i = 0; i < amountOfJewels; i++)
			{
				for (short j = 0; j < amountOfJewels; j++)
				{
					if (gameBackboard.jewels[i][j].active && !gameBackboard.jewels[i][j].selected)
					{
						if (checkJewelMouseCollision(GetMousePosition(), gameBackboard.jewels[i][j]))
						{
							if (!jewelSelected)
							{
								gameBackboard.jewels[i][j].selected = true;

								jewelsSelectedPos[auxIndex].x = j;
								jewelsSelectedPos[auxIndex].y = i;

								jewelSelected = true;
							}
							else
							{
								if (checkJewelsSelected(i, j))
								{
									gameBackboard.jewels[i][j].selected = true;

									auxIndex++;
									jewelsSelectedPos[auxIndex].x = j;
									jewelsSelectedPos[auxIndex].y = i;

								}
							}
						}
					}									
				}				
			}
		}

		static void pauseSelection()
		{
			if (GetMouseX() >= pauseButon.collider.x && GetMouseX() <= pauseButon.collider.x + pauseButon.collider.width)
			{
				if (GetMouseY() >= pauseButon.collider.y && GetMouseY() <= pauseButon.collider.y + pauseButon.collider.height)
				{
					game::actualScene = Scenes::Pause;
				}
			}
		}

		static void imput()
		{
			if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
			{
				sounds::playMovingMenu = true;
				
			}

			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
			{
				jewelSelection();
				pauseSelection();
			}
		}

		static void correctMousePosition()
		{
			if (GetMouseX() < 1)
			{
				SetMousePosition(1,GetMouseY());
			}

			if (GetMouseX()>window::screen.width-1)
			{
				SetMousePosition(window::screen.width-1, GetMouseY());
			}

			if (GetMouseY() < 1)
			{
				SetMousePosition(GetMouseX(), 1);
			}

			if (GetMouseY() > window::screen.height - 1)
			{
				SetMousePosition(GetMouseX(), window::screen.height-1);
			}
		}

		static void addPoints(int jewelCounter)
		{
			gameBackboard.score += jewelCounter * pointsPerJewel;
		}

		static void takeMoves()
		{
			gameBackboard.moves--;
			if (gameBackboard.moves <=0)
			{
				gameOver = true;
			}
		}

		static void subtracObjectives(jewelType type,int amount)
		{
			for (short i = 0; i < amount; i++)
			{
				switch (type)
				{
				case jewelType::Blue:

					if (gameBackboard.blueObj.amountToReach > 0)
					{
						gameBackboard.blueObj.amountToReach--;
					}
					
					break;
				case jewelType::Red:

					if (gameBackboard.redObj.amountToReach > 0)
					{
						gameBackboard.redObj.amountToReach--;
					}
					
					break;
				case jewelType::Green:

					if (gameBackboard.greenObj.amountToReach > 0)
					{
						gameBackboard.greenObj.amountToReach--;
					}
					
					break;
				case jewelType::Grey:

					if (gameBackboard.greyObj.amountToReach > 0)
					{
						gameBackboard.greyObj.amountToReach--;
					}
					
					break;
				default:
					break;
				}

			}
		}

		static void collectSelectedJewel()
		{
			int jewelCounter = 0;

			for (short i = 0; i < amountOfJewels; i++)
			{
				for (short j = 0; j < amountOfJewels; j++)
				{
					if (gameBackboard.jewels[i][j].selected)
					{
						jewelCounter++;
					}
				}
			}

			if (jewelCounter>=minimunAmountOfJewelToJoin)
			{
				jewelType auxJewelType;
				for (short i = 0; i < amountOfJewels; i++)
				{
					for (short j = 0; j < amountOfJewels; j++)
					{
						if (gameBackboard.jewels[i][j].selected)
						{
							gameBackboard.jewels[i][j].active = false;
							auxJewelType = gameBackboard.jewels[i][j].type;
						}
					}
				}

				addPoints(jewelCounter);
				subtracObjectives(auxJewelType, jewelCounter);
			}
			else
			{
				for (short i = 0; i < amountOfJewels; i++)
				{
					for (short j = 0; j < amountOfJewels; j++)
					{
						if (gameBackboard.jewels[i][j].selected)
						{
							gameBackboard.jewels[i][j].selected = false;
						}
					}
				}
			}

			takeMoves();
		}

		static void selectionTimer()
		{
			if (jewelSelected)
			{
				turnTimer += GetFrameTime();				
				if (turnTimer >= timePerTurn)
				{
					initialize::initTimer(turnTimer);
					collectSelectedJewel();
					jewelSelected = false;
					sounds::playAccepMenu = true;
				}
			}
		}

		static void jewelsRegeneration()
		{
			regenerateTimer += GetFrameTime();
			if (regenerateTimer>=timeForNewGems)
			{
				initialize::initTimer(regenerateTimer);
				
			}

			for (short i = 0; i < amountOfJewels; i++)
			{
				for (short j = 0; j < amountOfJewels; j++)
				{
					if (!gameBackboard.jewels[i][j].active)
					{						
						for (short k = i; k >= 1; k--)
						{							
							gameBackboard.jewels[k][j].active = gameBackboard.jewels[k - 1][j].active;
							gameBackboard.jewels[k][j].type = gameBackboard.jewels[k - 1][j].type;
							gameBackboard.jewels[k][j].selected = gameBackboard.jewels[k - 1][j].selected;							
						}
						jewels::generateJewelType(gameBackboard.jewels[0][j]);
					}
				}

			}
			
		}

		static void gameOverCheck()
		{
			if (gameOver)
			{
				game::actualScene = Scenes::MainMenu;
				deinit();
			}
		}

		static void levelUp()
		{
			gameBackboard.level++;

			gameBackboard.blueObj.amountToReach = gameBackboard.level * 10;
			gameBackboard.redObj.amountToReach = gameBackboard.level * 10;
			gameBackboard.greenObj.amountToReach = gameBackboard.level * 10;
			gameBackboard.greyObj.amountToReach = gameBackboard.level * 10;

			gameBackboard.moves = gameBackboard.level * 10;
		}

		static void levelUpCheck()
		{
			if (gameBackboard.blueObj.amountToReach == 0 && gameBackboard.redObj.amountToReach == 0 && gameBackboard.greenObj.amountToReach == 0 && gameBackboard.greyObj.amountToReach == 0)
			{
				levelUp();
			}
		}
	}

	namespace drawing
	{
		static void drawBackground()
		{
			DrawTexture(background,backgroundPos.x,backgroundPos.y,backgoundDye);
		}

		static void drawObjetives(Objectives objective)
		{
			DrawTexture(objective.texture,objective.collider.x, objective.collider.y,WHITE);
			DrawText(TextFormat("%i",objective.amountToReach), objective.collider.x + objective.collider.width*1.5f, objective.collider.y + (objective.collider.height/2) - (gameBackboard.texSize/2), gameBackboard.texSize, texColor);
		}

		static void drawUI()
		{
			DrawText("Objectives", (window::screen.width / 3) * 2 - (text::measureStringText((string)("Objectives"), gameBackboard.texSize) / 10), (window::screen.height / 30), gameBackboard.texSize*0.75f, texColor);
			drawObjetives(gameBackboard.blueObj);
			drawObjetives(gameBackboard.redObj);
			drawObjetives(gameBackboard.greenObj);
			drawObjetives(gameBackboard.greyObj);

			DrawText(TextFormat("Moves: %i", gameBackboard.moves), gameBackboard.movesPos.x, gameBackboard.movesPos.y, gameBackboard.texSize/2, texColor);
			DrawText(TextFormat("Score: %i", gameBackboard.score), gameBackboard.scorePos.x, gameBackboard.scorePos.y, gameBackboard.texSize/2, texColor);
			DrawText(TextFormat("Level: %i", gameBackboard.level), gameBackboard.levelPos.x, gameBackboard.levelPos.y, gameBackboard.texSize/2, texColor);
		}

		static void drawBackboard()
		{
			for (short i = 0; i < amountOfJewels; i++)
			{
				for (short j = 0; j < amountOfJewels; j++)
				{
					Color auxDye;
					Texture2D auxTexture = blueJewel;

					if (gameBackboard.jewels[i][j].active)
					{
						if (!gameBackboard.jewels[i][j].selected)
						{
							auxDye = gameBackboard.jewels[i][j].dye;
						}
						else
						{
							auxDye = auxSelectionDye;
						}

						switch (gameBackboard.jewels[i][j].type)
						{
						case jewelType::Blue:
							auxTexture = blueJewel;
							break;
						case jewelType::Red:
							auxTexture = redJewel;
							break;
						case jewelType::Green:
							auxTexture = greenJewel;
							break;
						case jewelType::Grey:
							auxTexture = greyJewel;
							break;
						default:
							break;
						}
						DrawTexture(auxTexture, gameBackboard.jewels[i][j].collider.x, gameBackboard.jewels[i][j].collider.y, auxDye);
					}					
				}				
			}		

			drawUI();
			
		}

		static void drawPauseButton()
		{
			DrawTexture(pauseButon.texture,pauseButon.collider.x,pauseButon.collider.y,WHITE);
		}
	}

	namespace deinitialize
	{
		static void deinitBackground()
		{
			UnloadTexture(background);			
		}		

		static void deinitObjectives()
		{
			UnloadTexture(gameBackboard.blueObj.texture);
			UnloadTexture(gameBackboard.redObj.texture);
			UnloadTexture(gameBackboard.greenObj.texture);
			UnloadTexture(gameBackboard.greyObj.texture);
		}

		static void deinitBackboard()
		{
			UnloadTexture(blueJewel);
			UnloadTexture(redJewel);
			UnloadTexture(greenJewel);
			UnloadTexture(greyJewel);
		}

		static void deinitPauseButton()
		{
			UnloadTexture(pauseButon.texture);
		}
	}

	static void init()
	{
		using namespace initialize;
		initBackground();
		initBackboard();				
		mouse::init();

		jewelSelected = false;
		
		initSelectedPos();

		initTimer(turnTimer);
		initTimer(regenerateTimer);

		initPauseButon();
		gameOver = false;
	}

	static void update()
	{
		using namespace updating;
		mouse::update();
		imput();
		correctMousePosition();
		selectionTimer();
		jewelsRegeneration();
		gameOverCheck();
		levelUpCheck();
	}

	void draw()
	{
		using namespace drawing;

		drawBackground();
		drawBackboard();
		drawPauseButton();
		mouse::draw();
	}

	void play()
	{
		if (!gameInitialized)
		{
			init();
			gameInitialized = true;
		}

		update();
	}

	void deinit()
	{
		using namespace deinitialize;

		gameInitialized = false;
		deinitBackground();
		deinitBackboard();
		deinitPauseButton();
				
		mouse::deinit();
	}
}