#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "Entities/jewels.h"


const int amountOfJewels = 15;

struct Buttons
{
	Texture2D texture;
	Rectangle collider;
};

struct Objectives
{
	int amountToReach;
	Texture2D texture;
	Rectangle collider;
	
};

struct Backboard
{
	int moves;
	Vector2 movesPos;

	int score;
	Vector2 scorePos;

	int level;
	Vector2 levelPos;	

	Objectives blueObj;
	Objectives redObj;
	Objectives greenObj;
	Objectives greyObj;

	int texSize;

	Jewel jewels[amountOfJewels][amountOfJewels];

};

namespace gameplay
{
	const float timePerTurn = 5.000f; //Expressed in miliseconds
	const float timeForNewGems = 10.000f;
	const int minimunAmountOfJewelToJoin = 3;
	const int pointsPerJewel = 100;

	const Color texColor = BLACK;

	extern bool gameInitialized;
	const Color auxSelectionDye = BLACK;
	void draw();
	void play();
	void deinit();
}

#endif // !GAMEPLAY_H

