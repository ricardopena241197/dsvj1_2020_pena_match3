#ifndef JEWELS_H
#define JEWELS_H
#include "raylib.h"

enum class jewelType
{
	Blue,
	Red,
	Green,
	Grey
};
const int amounOfTypes = 4;
struct Jewel
{	
	Color dye;
	jewelType type;
	bool active;
	bool selected;
	Rectangle collider;
};

namespace jewels
{
	void generateJewelType(Jewel& jewel);
}

#endif // !JEWELS_H

