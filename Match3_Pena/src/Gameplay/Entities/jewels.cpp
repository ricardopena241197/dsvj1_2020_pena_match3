#include "jewels.h"

#include <iostream>
#include <time.h>

namespace jewels
{
	void generateJewelType(Jewel & jewel)
	{
		jewel.type = (jewelType)(rand() % amounOfTypes);
		jewel.active = true;
		jewel.selected = false;
		jewel.dye = WHITE;
	}
}