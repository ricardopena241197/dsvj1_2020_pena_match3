#include "mouse.h"
#include "Textures/textures.h"
#include "Window/window.h"

namespace mouse
{
	bool mouseInitialized = false;
	GameplayMouse mouse;

	void init()
	{
		using namespace textures;
		
		
		mouse.texture = LoadTexture(mouseTexturePath);
		mouse.texture.height = window::screen.height / 12;
		mouse.texture.width = window::screen.width / 17;

		SetMousePosition(window::screen.width/2, window::screen.height/2);
		mouse.position = GetMousePosition();

		mouse.dye = WHITE;
	}

	void update()
	{
		if (!mouseInitialized)
		{
			mouseInitialized = true;
		}

		mouse.position = GetMousePosition();
	}

	void draw()
	{
		DrawTexture(mouse.texture,mouse.position.x,mouse.position.y,mouse.dye);
	}

	void deinit()
	{
		mouseInitialized = false;
		DisableCursor();
	}
}