#ifndef MOUSE_H
#define MOUSE_H
#include "raylib.h"

struct GameplayMouse
{
	Texture2D texture;
	Vector2 position;
	Color dye;
};

namespace mouse
{
	void init();
	void update();
	void draw();
	void deinit();
}

#endif // !MOUSE_H

